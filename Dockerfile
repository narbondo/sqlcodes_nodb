FROM fabric8/java-alpine-openjdk8-jdk

# indica el jar a ejecutar al iniciar el contenedor
ENV JAVA_APP_JAR sqlcodes_nodb-1.0-SNAPSHOT-runner.jar

# desactiva jolokia
ENV AB_OFF true                                 

# añade el jar a la imágen
ADD target/sqlcodes_nodb-1.0-SNAPSHOT-runner.jar /deployments/  
