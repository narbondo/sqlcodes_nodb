# Consulta de sqlcodes

Una vez levantado el servidor disponemos de dos endpoints:
* http://localhost:8080/sqlcode: muestra una tabla con todos los sqlcodes
* http://localhost:8080/sqlcode/codigo: devuelve un json con la descripción del código

Para usar el contenedor: docker push rlnieto/sqlcodes
